Fork of [JOpenSurf](https://code.google.com/p/jopensurf/) project with fixes for CARDS project

# Maven #

To use as maven dependency add repository:

```xml
        <repository>
            <id>maven.uits-labs.ru</id>
            <name>maven.uits-labs.ru</name>
            <url>http://maven.uits-labs.ru/</url>
        </repository>
```

And dependency:

```xml
        <dependency>
            <groupId>fi.oulu.cards</groupId>
            <artifactId>jopensurf</artifactId>
            <version>20160907</version>
        </dependency>
```