package com.stromberglabs.jopensurf;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.imageio.ImageIO;

import junit.framework.Assert;
import org.junit.Test;


public class SurfTest {

    @Test
    public void testOneAndTwoSteps() throws Exception {

        InputStream inputStream = this.getClass().getResourceAsStream("/lenna.png");
        BufferedImage image = ImageIO.read(inputStream);
        inputStream.close();

        Surf one1 = new Surf(image, 0.21F, 4.0E-6F, 5);

        Surf two = new Surf(image, 0.21F, 4.0E-6F, 5);

        List<SURFInterestPoint> points1 = one1.getFreeOrientedInterestPoints();

        List<SURFInterestPoint> positions = two.getDescriptorFreeInterestPoints();

        Assert.assertEquals(points1.size(), positions.size());

        for (SURFInterestPoint position : positions) {
            two.fillPoint(position, false);
        }

        for (int i = 0; i < points1.size(); i++) {
            Assert.assertTrue(positions.get(i).isEquivalentTo(points1.get(i)));
        }

    }


    @Test
    public void testSingular() throws Exception {

        InputStream inputStream = this.getClass().getResourceAsStream("/singularImage1.png");
        BufferedImage image = ImageIO.read(inputStream);
        inputStream.close();

        Surf current = new Surf(image, 0.21F, 4.0E-6F, 5);

        List<SURFInterestPoint> interestPoints = current.getFreeOrientedInterestPoints();

        ObjectInputStream objectInputStream = new ObjectInputStream(this.getClass().getResourceAsStream("/points.ser"));
        ArrayList<SURFInterestPoint> surfInterestPoints = new ArrayList<SURFInterestPoint>(100);
        while(true){
            try {
                surfInterestPoints.add((SURFInterestPoint) objectInputStream.readObject());
            }
            catch (EOFException e){
                break;
            }
        }


        for (int i = 0; i < surfInterestPoints.size(); i++) {
           Assert.assertTrue(interestPoints.get(i).isEquivalentTo(surfInterestPoints.get(i)));
        }


    }

    private void savePoints(List<SURFInterestPoint> interestPoints, String name) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(name));
        for (SURFInterestPoint interestPoint : interestPoints) {
            outputStream.writeObject(interestPoint);
        }
        outputStream.close();
    }

    private AtomicLong atomicLong = new AtomicLong(0);
    private AtomicLong subimage = new AtomicLong(0);

    private void trySingular(BufferedImage image) {
        try {
            ImageIO.write(image, "png", new File("subimage" + subimage.incrementAndGet() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            Surf current = new Surf(image, 0.21F, 4.0E-6F, 5);

            List<SURFInterestPoint> interestPoints = current.getFreeOrientedInterestPoints();

            System.out.println("interestPoints = " + interestPoints);
        } catch (org.apache.commons.math3.linear.SingularMatrixException e) {
            try {
                ImageIO.write(image, "png", new File("singularImage" + atomicLong.incrementAndGet() + ".png"));

                int w = image.getWidth();
                int h = image.getHeight();
                if(w > 5 && h > 5) {
                    trySingular(image.getSubimage(0, 0, w, h / 2));
                    trySingular(image.getSubimage(0, h / 2, w, h / 2));
                    trySingular(image.getSubimage(0, 0, w / 2, h));
                    trySingular(image.getSubimage(w / 2, 0, w / 2, h));

                    trySingular(image.getSubimage(w / 4, 0, w / 2, h));
                    trySingular(image.getSubimage(0, h / 4, w, h / 2));

                }


            } catch (IOException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

}
